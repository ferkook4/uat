public class sendMailExp {
    @auraEnabled
    public static void sendMail(String email, String msg){
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        String[] sendingTo = new String[]{email}; 
        semail.setToAddresses(sendingTo);
        semail.setSubject('Ejemplo email'); 
        semail.setPlainTextBody(msg); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail}); 
    }
}