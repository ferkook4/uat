({
	sendMsg : function(component, email, msg) {
		var action=component.get('c.sendMail');
        action.setParams({
            email: email,
            msg: msg
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The message has been sent.",
                    "type": "success"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}, 
    validateFields: function(email, msg){
        var empty;
        if($A.util.isEmpty(email)||$A.util.isEmpty(msg)){
            empty=true;
        } 
        return empty;
    }
})